package net.xeill.elpuig;

import java.util.Random;
import java.util.Scanner;

class IO {

  Scanner input = new Scanner(System.in);

  public void cls() {
    System.out.print("\033\143");
  }

  public void welcome() {
    System.out.println("Welcome to Puigmania");
    System.out.println("---Q---U---E---D---A---T---E---E---N---C---A---S---A---!");

  }
  public void youLose() {
    System.out.println("You limp out of the puig, weat from battle");
  }

  public void youWin() {
    System.out.println("You Win");
  }


  public int rnd(int min, int max) {
    Random r = new Random();
    return r.nextInt((max - min) + 1) + min;
  }

  public void enemyAppeared(Enemy enemy) {
    System.out.println("\tª" + enemy.name + "  " + "appeared! ª\n");
  }

  public void invalidCommand() {
    System.out.println("\t>Invalid Command!");
  }

  public void runAway(Enemy enemy) {
    System.out.println("\t> You run away From the " + enemy.name + "!");
  }

  public void tooWeak() {
    System.out.println("\t> You have recieve too much damage, you are too weak.");
  }


  public void menuPlayerChoice() {
    System.out.println("\n\tWhat WOuld you Like to do?");
    System.out.println("\t1. Attack");
    System.out.println("\t2. Drink Health Potion");
    System.out.println("\t3. Run");
  }

  public int getMenuPlayerChoice() {
      String soption;
      int option;
      try {
        soption = input.nextLine();
        option = Integer.parseInt(soption);
      } catch (Exception e) {
        // Si ha fallado porque ha introducido un valor que no es entero
        // Se borra la pantalla
        cls();
        // Se imprime el menú de inicio
        menuPlayerChoice();
        // Se vuelve a llamar para capturar a sí misma para intentar leer el entero
        return getMenuPlayerChoice();
      }
      return option;
  }

}
