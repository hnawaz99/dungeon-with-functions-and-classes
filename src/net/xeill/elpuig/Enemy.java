package net.xeill.elpuig;

 class Enemy {

   // Atributos
   String name;
   int health;
   int attackDamage;

   // Constructor
   Enemy(String name, int health, int attackDamage) {
       this.name = name;
       this.health = health;
       this.attackDamage = attackDamage;
   }

   public boolean isAlive() {
     return (health > 0);
   }

   public void info() {
     System.out.println("\t" + name + "HP: " + health);
   }

   public void impact(int power) {
     health -= power;
     System.out.println("\t>You strike " + name + " for " + power + " damage. ");
   }





 }
