package net.xeill.elpuig;

import java.util.Scanner;

class Game {

  IO io = new IO();
  Player player = new Player("Ninja");
  boolean isOver = false;

  public void start() {

    io.welcome();

    while (!isOver) {

      // Crear un enemigo random
      Enemy enemy = createRandomEnemy();
      io.enemyAppeared(enemy);
      boolean runAway = false;

        while (enemy.isAlive() && !runAway) {

            player.info();
            io.menuPlayerChoice();

            int option = io.getMenuPlayerChoice();

            switch(option) {
              case 1:
                fight(enemy);
                if (!player.isAlive()) {
                  io.tooWeak();
                  setGameOver();
                }
                break;

              case 2:
                if (!player.drinkPotion()) {
                  //setGameOver();
                  // Last chance
                  fight(enemy);
                  if (enemy.isAlive()) {
                    // You Lose
                    io.youLose();
                  } else {
                    // You win
                    io.youWin();
                  }
                  setGameOver();
                }
                break;

              case 3:
                io.runAway(enemy);
                runAway = true;
                break;

              default:
                io.invalidCommand();
                break;
            }
      }
    }
  }

  public Enemy createRandomEnemy() {
    String[] enemies = {"Skeleton", "Zombies", "Warrior", "Assassin"};
    String name = enemies[io.rnd(0, enemies.length-1)];
    Enemy e = new Enemy(name, io.rnd(75,100), io.rnd(25,50));

    return e;
  }

  public void fight(Enemy enemy) {
    player.impact(io.rnd(25,50));
    enemy.impact(io.rnd(25,50));
  }

  public void setGameOver() {
    isOver = true;
  }

  public boolean isGameOver() {
    return isOver;
  }

}
