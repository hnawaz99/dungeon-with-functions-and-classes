package net.xeill.elpuig;

 class Player {

   // Atributos
   String name;
   int health = 100;
   int attackDamage = 50;
   int healthPotions = 3;
   int healthPotionHealAmount = 30;
   int healthPotionDropChance = 50;

   // Constructor
   Player(String name) {
       this.name = name;
   }

   public boolean isAlive() {
     return (health > 0);
   }

  public void info() {
    System.out.println("\tYour HP; " + health);
  }

  public void impact(int power) {
    health -= power;
    System.out.println("\t> You recieve " + power + " Damage " + " in fight.");
  }

  public boolean drinkPotion() {
    if (healthPotions > 0) {
      health += healthPotionHealAmount;
      healthPotions--;
      System.out.println("\t>You Drink A potion, Healing For Yourself for " + healthPotionHealAmount + "."
      + "\n\tYou now have " + health + " HP "
      + "\n\t>You have " + healthPotions + " health potions left.\n ");
      return true;
    } else {
      System.out.println("\t> You have no health potion left Defeat enemies from a Chance to get!");
      return false;
    }
  }

}
